# stllas_gazebo

A Ros package for the Something that looks like a shoulder robot to be launched in gazebo

Install, and launch the launch file:
```
roslaunch stllas_gazebo world.launch
```

Something that looks like a shoulder with ROS/Gazebo
